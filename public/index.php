<?php
//public/index.php
require __DIR__ . './../vendor/autoload.php';

use Ascf\core\App;

define('APP_ROOT', __DIR__ . '/../src');
define('APP_CONFIG', APP_ROOT . '/config/config.php');

$app = App::getInstance();
$app->run();
