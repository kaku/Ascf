<?php
namespace Ascf\controllers;

class TestController extends Controller {
    public function index() {
        $data = [
            'message' => 'Hello, Ascf MVC Framework!',
        ];
        $this->view('test/index', $data);
    }
}
