<?php

namespace Ascf\Controllers;

use Ascf\Core\Controller;
use Ascf\Core\Response\Response;
use Ascf\models\User;

class BaseController  extends Controller {
    public function index($name) {
        $msg="Hello, welcome to Ascf {$name}!";
        return new Response($msg);
    }
    public function user() {
        $users = [
            new User('John Doe', 'john@example.com'),
            new User('Jane Doe', 'jane@example.com')
        ];

        $this->render('user/index', ['users' => $users]);
    }
}