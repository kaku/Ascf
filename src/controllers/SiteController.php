<?php
namespace Ascf\controllers;

use Ascf\Core\Controller;


class SiteController extends Controller {
    // public $viewDirectory='test';

    public function index() {
       
        $this->view('index', ["data"=>""]);
    }

    public function scan() {
        $dir = '/volume1/homes/xuezhileikaku/books/epub/';
        // $dir='E:/soft';
        $filePaths = [];
        $this->traverseDirectory($dir, function ($file) use (&$filePaths) {
            if ($file->isDir()) {
                $filePaths[] = 'Directory: ' . $file->getPathname();
            } else {
                $filePaths[] = 'File: ' . $file->getPathname();
            }
        });
        $this->view('site/index', ['filePaths' => $filePaths]);
    }

    private function traverseDirectory($dir, $callback) {
        $files = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator($dir, \RecursiveDirectoryIterator::SKIP_DOTS),
            \RecursiveIteratorIterator::SELF_FIRST
        );

        foreach ($files as $file) {
            $callback($file);
        }
    }
  
}
