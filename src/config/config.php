<?php
// config/config.php
// 引入应用程序配置
$appConfig = require 'app.php';
// 引入数据库配置
$dbConfig = require 'db.php';

// 引入 Redis 配置
$redisConfig = require 'redis.php';
$routes = require 'routes.php';

return [
    'app' => $appConfig, // 将应用程序配置添加到全局配置中
    'db' => $dbConfig,
    'redis' => $redisConfig, // 将 Redis 配置添加到全局配置中
    'route' => $routes,
    // 其他全局配置...
];
