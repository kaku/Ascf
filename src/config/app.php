<?php
return [
    'app_name' => "A simple cheng' framework for php",
    'app_type' => "web",
    'debug' => true,
    'timezone' => 'Asia/Shanghai',
    'locale' => 'zh_cn',
    'log' => 'warn',
    'date_timezone' => 'Asia/Shanghai',


// 其他应用程序相关配置...
];
