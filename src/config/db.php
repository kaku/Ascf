<?php

// config/db.php

return [
    'database_type' => 'mysql',
    'database_name' => 'nas',
    'server' => 'localhost',
    'username' => 'root',
    'password' => 'root',
    // 'password' => 'root@cheng',//nas

    // 可选配置
    'charset' => 'utf8mb4',
    'port' => 3306,
    'error' => PDO::ERRMODE_EXCEPTION,
    // 其他配置...
];
