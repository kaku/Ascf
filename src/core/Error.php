<?php

namespace Ascf\Core;

class Error extends \Exception {
    // 在这里，我们没有必要重写 Throwable 的方法，因为 Exception 已经实现了它们
    // 但是，我们可以添加额外的属性或方法

    public function __construct($message = "General Error", $code = 0, \Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
    }

    // 例如，添加一个方法来格式化错误信息
    public function getFormattedError() {
        return sprintf("[%s:%d] %s", $this->getFile(), $this->getLine(), $this->getMessage());
    }
}