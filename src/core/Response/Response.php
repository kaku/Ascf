<?php

namespace Ascf\Core\Response;

use Ascf\Core\AppFactory;
use Ascf\Core\ResponseInterface;
use FastRoute\Dispatcher;
use Invoker\Invoker;

class Response implements ResponseInterface
{
    private $statusCode;
    private $headers;
    private $body;

    public function __construct($body = '', $statusCode = 200, array $headers = [])
    {
        $this->statusCode = $statusCode;
        $this->headers = $headers;
        $this->body = $body;
    }

    public function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;
    }

    public function addHeader($name, $value)
    {
        $this->headers[$name] = $value;
    }

    public function send()
    {
        if (!headers_sent()) {
            http_response_code($this->statusCode);
            foreach ($this->headers as $name => $value) {
                header("$name: $value");
            }
        }
        echo $this->body;
//        return $this->body;
    }

    public function resolve($routeInfo)
    {
        if($routeInfo[0]===Dispatcher::FOUND){
            $handler = $routeInfo[1];
            $vars = $routeInfo[2];
            // 检查处理器是闭包还是字符串（控制器方法）
            if (is_callable($handler)) {
                // 直接调用闭包
                $this->body = call_user_func_array($handler, $vars);
            } else {
                // 字符串
                $this->body=$handler;
            }
        }else{
            switch ($routeInfo[0]) {
                case Dispatcher::NOT_FOUND:
                    $this->body = '404 Not Found';
                    $this->statusCode = 404;
                    break;

                case Dispatcher::METHOD_NOT_ALLOWED:
                    $this->body = '405 Method Not Allowed';
                    $this->statusCode = 405;
                    break;

                default:
                    $this->body = 'Internal Server Error';
                    $this->statusCode = 500;
                    break;
            }
        }

        // 设置合适的头部
        $this->addHeader('Content-Type', 'text/plain');

        // 发送响应
        return $this->send();
    }


    public function redirect($to, $status = 302, array $headers = array())
    {
        return new RedirectResponse($to, $status, $headers);
    }

    public function json($data, $status = 200, array $headers = array())
    {
        return new JsonResponse($data, $status, $headers);
    }

    public function standard($content = '', $status = 200, array $headers = array())
    {
        return new Response($content, $status, $headers);
    }

    public function file($file, $status = 200, $headers = array(), $public = true, $contentDisposition = null, $autoEtag = false, $autoLastModified = true)
    {
        return new BinaryFileResponse($file, $status, $headers, $public, $contentDisposition, $autoEtag, $autoLastModified);
    }

    public function streamed($callback = null, $status = 200, $headers = array())
    {
        return new StreamedResponse($callback, $status, $headers);
    }
}