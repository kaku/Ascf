<?php
// core/AppFactory.php
namespace Ascf\Core;

use Ascf\Core\Response\Response;
use DI\ContainerBuilder;

class AppFactory
{
    private $diContainer;

    public function __construct($config)
    {

        $containerBuilder = new ContainerBuilder();
        $this->diContainer = $containerBuilder->build();
        $this->router = new Router($config['route']);
        $this->initializeBindings();
    }

    private function initializeBindings()
    {
        // 绑定配置文件
        $this->diContainer->set('config', function () {
            return require APP_CONFIG;
        });

        // 绑定 Request 和 Response
        $this->diContainer->set('request', \DI\create(Request::class));
        $this->diContainer->set('response', \DI\create(Response::class));
        // 绑定 Router
        $this->diContainer->set('router', $this->router);


        // ...其他绑定...
    }

    public function bind($abstract, $concrete, $singleton = true)
    {
        // 在 PHP-DI 7.0 中，默认为单例
        $this->diContainer->set($abstract, \DI\create($concrete));
    }

    public function make($abstract)
    {
        try {
            return $this->diContainer->get($abstract);
        } catch (\Exception $e) {
            throw $e;
        }
    }

    public function getContainer()
    {
        return $this->diContainer;
    }
}


