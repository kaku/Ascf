<?php
namespace  Ascf\Core;

use Psr\Http\Message\ResponseInterface;
use Laminas\Diactoros\Response;

class Controller {

    protected function render($view, $data = []) {
        extract($data);

        include "Views/$view.php";
    }

}
