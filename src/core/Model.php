<?php
// src/core/Model
namespace Ascf\Core;

use Medoo\Medoo;

class Model {
    protected $db;

    public function __construct($dbConfig) {
        $this->db = new Medoo($dbConfig);
    }

    // 获取一条数据
    public function one($table, $columns, $where) {
        return $this->db->get($table, $columns, $where);
    }

    // 获取所有数据
    public function all($table, $columns, $where = null) {
        return $this->db->select($table, $columns, $where);
    }

    // 添加数据
    public function add($table, $data) {
        return $this->db->insert($table, $data);
    }

    // 更新数据
    public function update($table, $data, $where) {
        return $this->db->update($table, $data, $where);
    }

    // 删除数据
    public function delete($table, $where) {
        return $this->db->delete($table, $where);
    }

    // 带条件查询
    public function where($table, $columns, $where) {
        return $this->db->select($table, $columns, $where);
    }
    // ... 可以根据需要添加更多方法 ...
}
