<?php

namespace Ascf\Core;

use Ascf\Core\Response\Response;
use FastRoute\RouteCollector;
use function FastRoute\simpleDispatcher;

class Router
{
    private $dispatcher;

    public function __construct($config)
    {
        $this->dispatcher = simpleDispatcher(function (RouteCollector $r) use ($config) {
            foreach ($config as $route) {
                list($httpMethod, $path, $handler) = $route;
                $r->addRoute($httpMethod, $path, $handler);
            }
        });
    }

    public function dispatch($method, $uri)
    {
        return $this->dispatcher->dispatch($method, $uri);
    }

}