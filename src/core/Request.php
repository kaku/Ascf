<?php

namespace Ascf\Core;


class Request {
    private $queryParams = [];
    private $parsedBody;
    private $serverParams;
    private $uri;
    private $method;

    public function __construct(array $queryParams = [], array $parsedBody = [], array $serverParams = []) {
        $this->queryParams = $queryParams;
        $this->parsedBody = $parsedBody;
        $this->serverParams = $serverParams;
        $this->uri = $serverParams['REQUEST_URI'] ?? '/';
        $this->method = $serverParams['REQUEST_METHOD'] ?? 'GET';
    }

    public static function fromGlobals() {
        return new self($_GET, $_POST, $_SERVER);
    }

    public function getQueryParam($key, $default = null) {
        return $this->queryParams[$key] ?? $default;
    }

    public function getParsedBody() {
        return $this->parsedBody;
    }

    public function getUri() {
        return $this->uri;
    }

    public function getMethod() {
        return $this->method;
    }
}
