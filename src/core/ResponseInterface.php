<?php

namespace Ascf\Core;

interface ResponseInterface
{
    public function send();
}
