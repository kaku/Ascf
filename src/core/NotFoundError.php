<?php

namespace Ascf\Core;

use DI\NotFoundException;

class NotFoundError extends NotFoundException
{
    public function __construct($message = "Not Found", $code = 404, \Throwable $previous = null) {
        parent::__construct($message, $code, $previous);
    }
}