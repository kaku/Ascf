<?php
use PHPUnit\Framework\TestCase;

class Test extends TestCase {
    public function testAscf() {
        // 设置 PHP 输出缓冲区
        ob_start();

        // 模拟请求，访问 TestController 的 index 方法
        $_GET['url'] = 'test/index';
        require 'index.php';

        // 获取输出缓冲区内容
        $output = ob_get_clean();

        // 验证输出是否包含预期的消息
        $this->assertStringContainsString('Hello, Ascf MVC Framework!', $output);
    }
}
